'use strict'

const modalAdd = document.querySelector('.modal__add'),
  addAd = document.querySelector('.add__ad'),
  btnModalSubmit = modalAdd.querySelector('.modal__btn-submit'),
  modalSubmitForm = document.querySelector('.modal__submit'),
  catalog = document.querySelector('.catalog'),
  modalItem = document.querySelector('.modal__item'),
  modalBtnWarning = document.querySelector('.modal__btn-warning');

const dateBase = [];

// получаем все элементы формы
const elementsModalSubmit = [...modalSubmitForm.elements].filter(inp => ['INPUT', 'SELECT', 'TEXTAREA'].includes(inp.tagName));

// обрабатываем форму
modalSubmitForm.addEventListener('input', event => {
  const validForm = elementsModalSubmit.every(inp => inp.value);
  btnModalSubmit.disabled = !validForm;
  modalBtnWarning.style.display = validForm ? 'none' : '';
});

modalSubmitForm.addEventListener('submit', event => {
  event.preventDefault();
  const itemObj = {};
  for(const inp of elementsModalSubmit) {
    itemObj[inp.name] = inp.value;
  };
  dateBase.push(itemObj);
  modalSubmitForm.reset();

});

// правильная версия Макса )
const closeModalEscape = event => {
  if(event.key === "Escape") {
    if(!modalAdd.classList.contains('hide'))
      modalAdd.classList.add('hide');
    if(!modalItem.classList.contains('hide'))
      modalItem.classList.add('hide');
    document.removeEventListener('keydown', closeModalEscape);
  }
};

// открываем модальное окно по добавлению карт
addAd.addEventListener('click', () => {
  modalAdd.classList.remove('hide');
  btnModalSubmit.disabled = true;
  document.addEventListener('keydown', closeModalEscape);
});

/// !!! Новая домашка объединить функции closeModal и closeModalEscape !!!
// закрываеи мождалки, через this определяем элемент по которому мы кликнули
const closeModal = function(event){
  if(event.target.closest(".modal__close") || event.target === this){
    this.classList.add('hide');
    document.removeEventListener('keydown', closeModalEscape);
    if(this === modalAdd) modalSubmitForm.reset();
  }
}
// Домашнее задание 1.1
// открываем модальное окно при клике на саму карточку объявления
catalog.addEventListener("click", event => {
  const target = event.target;
  if(target.closest(".card")){
    modalItem.classList.remove('hide');
    document.addEventListener('keydown', closeModalEscape);
  }
});

modalItem.addEventListener("click", closeModal);
modalAdd.addEventListener("click", closeModal);

// Закрывам модалки по клавищи escape - Домашнее задание - 1.2
// document.addEventListener('keydown', event => {
//   if(event.key === "Escape") {
//     if(!modalAdd.classList.contains('hide'))
//       modalAdd.classList.add('hide');
//     if(!modalItem.classList.contains('hide'))
//       modalItem.classList.add('hide');
//   }
// });